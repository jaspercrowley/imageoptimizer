// This file is required by the index.html file and will
// be executed in the renderer process for that window.
// All of the Node.js APIs are available in this process.

const {dialog, BrowserWindow} = require("electron").remote;
const ImageOpitimizer = require('./src/ImageOptimizer/ImageOptimizer');
const TabWindows = [
    new ImageOpitimizer('image-optimizer-tab')
];
const config = require('./settings.json').settings;
const activeTab = TabWindows[config.activeTab];

document.querySelector('#minbtn').addEventListener('click', (e) => BrowserWindow.getFocusedWindow().minimize());
document.querySelector('#maxbtn').addEventListener('click', (e) => BrowserWindow.getFocusedWindow().maximize());
document.querySelector('#closebtn').addEventListener('click', (e) => BrowserWindow.getFocusedWindow().close());
document.ondragover = document.ondrop = (event) => {event.preventDefault();};

window.addEventListener('resize', () => {
    console.log(window.outerHeight);
    document.querySelector('.images-panel').style.maxHeight = `${window.innerHeight-100}px`;
});

SetTab = (tab) => { 
    document.querySelector('section').appendChild(tab.html); 
    // tab.initializeEvents();
}
SetTab(activeTab);

