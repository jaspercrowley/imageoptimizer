const fs = require('fs');
const Jimp = require("jimp");
const {dialog} = require("electron").remote;
const Components = require('../components/Components');
const {Panel, Label, TabWindow, Button, Range, CheckBox, TextBox, 
    NumberBox, DropDown, Picture, Folder, ImageContainer, Progress} = new Components();

module.exports = class ImageOptimizer extends TabWindow {
    constructor(classList) {
        super(classList);
        Folder.folders = [];
        this.addComponents();
        this.loadComponents();
    }

    addComponents() {
        this.tabTitle = new Label('h3', '', '', 'tab-window-title');
        this.addFolderButton = new Button('', 'Add folder', 'add-folder-button');
        this.addFolderButton.click(() => this.addFolder());
        this.optionsTitle = new Label('h4', '', 'Image Options');
        this.qualityLabel = new Label('span', '', 'Quality: 75%');
        this.qualityRange = new Range(75, 100, 1, 'quality-range');
        this.qualityRange.input((event) => {this.qualityLabel.html.innerText = `Quality: ${event.target.value}%`});
        this.scaleCheckBox = new CheckBox(false, 'Scale', 'scale-checkbox');
        this.scaleCheckBox.change((event) => {this.scalePanel.animate(event.target.checked)});
        this.scaleSizeInput = new NumberBox('Size', 1, 8096, '1024', 'scale-size');
        this.scaleSizeInput.input();
        this.scalePanel = new Panel(this, 'scale-panel', [this.scaleSizeInput.html]);
        this.saveAsTitle = new Label('h4', '', 'Save as');
        this.renameFilesCheckBox = new CheckBox(false, 'Rename files', 'rename-checkbox');
        this.renameFilesCheckBox.change((event) => {this.renamePanel.animate(event.target.checked)});
        this.renameTextBox = new TextBox('eg. filename', '', 'rename-files');
        this.renamePanel = new Panel(this, 'rename-panel', [this.renameTextBox.html]);
        this.fileTypeCheckBox = new CheckBox(false, 'File type', 'file-type-checkbox');
        this.fileTypeCheckBox.change((event) => {this.fileTypePanel.animate(event.target.checked)})
        this.fileTypeDropDown = new DropDown('File type', 'jpg', ['jpg', 'png'], 'file-type');
        this.fileTypePanel = new Panel(this, 'file-type-panel', [this.fileTypeDropDown.html]);
        this.destinationLabel = new Label('span', '', 'Destination', 'destination-label');
        this.destinationButton = new Button('', 'resized\\', 'destination-button');
        this.destinationButton.html.setAttribute('title', 'resized\\');
        this.destinationButton.click((event) => {
            const destination = dialog.showOpenDialog({properties: ['openDirectory']})[0];
            event.target.innerText = destination + '\\';
            this.destinationButton.html.setAttribute('title', destination + '\\');
            console.log(this.destinationButton.html.innerText);
        });
        this.optionsPanel = new Panel(this, 'image-options-panel', [
            document.createElement('br'),
            this.addFolderButton.html,
            this.optionsTitle.html,
            this.qualityLabel.html,
            this.qualityRange.html,
            this.scaleCheckBox.html,
            this.scalePanel.html,
            document.createElement('hr'),
            this.saveAsTitle.html,
            this.renameFilesCheckBox.html,
            this.renamePanel.html,
            this.fileTypeCheckBox.html,
            this.fileTypePanel.html,
            document.createElement('br'),
            this.destinationLabel.html,
            this.destinationButton.html
        ]);
        this.backgroundPanel = new Panel(this, 'background-panel', [this.optionsPanel.html]);
        this.dragLabel = new Label('h2', 'wallpaper', 'Drag files here', 'drag-label');
        this.imagesPanel = new Panel(this, 'images-panel', [this.dragLabel.html]);
        this.imagesPanel.html.ondragenter = (event) => {this.imagesPanel.html.style.background = '#d1d1d1'};
        this.imagesPanel.html.ondragleave = (event) => {this.imagesPanel.html.style.background = 'none'};
        this.imagesPanel.html.ondrop = (event) => {
            this.dropFiles(event.dataTransfer.files);
            this.imagesPanel.html.style.background = '#ffffff';
        };

        this.optimizeButton = new Button('', 'Optimize', 'optimize-button');
        this.optimizeButton.click(this.optimize);
        ImageOptimizer.progressBar = new Progress(0, 'Processing images please wait...');
        this.previewPanel = new Panel(this, 'preview-panel', [ImageOptimizer.progressBar.html]);
        this.components = [this.backgroundPanel.html, this.imagesPanel.html, this.optimizeButton.html, this.previewPanel.html];
        
    }

    addFolder() {
        let folderName = dialog.showOpenDialog({properties: ['openFile', 'openDirectory', 'multiSelections']})[0];
        fs.readdir(folderName, (err, files) => {
            if(!err) this.createFolder(folderName, files);
        });
    }

    dropFiles(files) {
        let folders = [];
        for (let i = 0; i < files.length; i++) {
            const fileName = files[i].name;
            const folderName = files[i].path.replace('\\' + fileName, '');
            const folder = folders.find(folder => { return folder.folder == folderName; });
            if(!folder) {
                folders.push({folder: folderName, files:[fileName]});
            } else {
                folder.files.push(fileName);
            }
        }
        folders.forEach(folder => this.createFolder(folder.folder, folder.files));
    }

    createFolder(folderName, files) {
        const images = Folder.filterFiles(files, ['.jpg', '.png']);
        if(images.length > 0) {
            const isFolder = Folder.findFolder(folderName);
            if(isFolder) {
                images.forEach((image, index) => {
                    if(this.findImage(isFolder, image)) {
                        const container = this.createPicture(folderName, image);
                        console.log(isFolder.html.children);
                        isFolder.html.children[0].appendChild(container.html);
                        isFolder.files.push(container);
                    }
                });
            } else {
                const closeButton = new Button('delete', '', 'folder-delete-button');
                closeButton.click(() => {
                    Folder.folders = Folder.folders.filter(f => f != folder);
                    folder.html.remove();
                });
                const label = new Label('span', 'folder', folderName, 'folder-label');
                const panel = new Panel(this, 'folder-panel', [label.html ,closeButton.html]);
                const imageContainers = [panel].concat(images.map((image) => {return this.createPicture(folderName, image)}));
                const folder = new Folder(folderName, folderName, imageContainers);
                label.html.addEventListener('click', () => {require('child_process').exec(`start "" "${folderName}"`)});
                Folder.folders.push(folder);
                document.querySelector('.images-panel').appendChild(folder.html);
            }
            
        }
    }
    createPicture(folderName, image) {
        const picture = new Picture(folderName, image);
        picture.setAttribute('data-src', `${folderName}\\${image}`);
        const closeButton = new Button('', 'X', 'container-delete-button');
        const panel = new Panel(this, 'container-panel', [closeButton.html]);
        const container = new ImageContainer(this, picture, 'image-container', [panel.html, picture]);
        container.click((event) => {
            if(!container.html.classList.contains('preview-image')) {
                container.html.classList += ' preview-image';
                closeButton.html.style.display = 'none';
            } else {
                container.html.classList.remove('preview-image');
                closeButton.html.style.display = 'block';
            }
        });
        closeButton.click(() => {
            const folder = Folder.folders.find((folder) => { return folder.location == folderName; });
            folder.files = folder.files.filter((file) => file != container);
            container.html.remove();
            if(folder.files.length == 0) {
                Folder.folders = Folder.folders.filter((f) => f != folder);
                folder.html.remove();
            }
        });
        return container;
    }
    
    // filterImages(files) {
    //     let images = [];
    //     files.forEach(file => {
    //         if(file.toLowerCase().endsWith('.jpg') || file.toLowerCase().endsWith('.png'))
    //             images.push(file);
    //     });
    //     return images;
    // }

    // findFolder(folderName) {
    //     let isFolder;
    //     if(Folder.folders.length > 0) {
    //         isFolder = Folder.folders.find(folder => { return folder.location == folderName; });
    //     } 
    //     return isFolder;
    // }

    findImage(folder, image) {
        let imageExist = folder.files.find(file => { 
            return file.classList != 'folder-panel' ? file.image.imageName == image : null;
        });
        if(!imageExist) {
            return true;
        } else {
            return false;
        }
    }

    optimize() {
        const quality = parseInt(document.querySelector('.quality-range').value);
        const scale = document.querySelector('.scale-checkbox').checked;
        const size = parseInt(document.querySelector('.scale-size').value);
        const renameFiles = document.querySelector('.rename-checkbox').checked;
        const fileName = document.querySelector('.rename-files').value;
        const changeFileType = document.querySelector('.file-type-checkbox').checked;
        const fileType = document.querySelector('.file-type').value;
        const destination = document.querySelector('.destination-button').innerText;
        let totalFiles = 0, currentFile = 0, fileIndex = 0;
        Folder.folders.forEach(folder => totalFiles += folder.files.length - 1);
        Folder.folders.forEach(folder => {
            folder.files.forEach(fileContainer => {
                if(fileContainer.html.className == 'folder-panel') {
                    console.log(fileContainer);
                } else {
                    document.querySelector('.preview-panel').style.display = 'flex';
                    
                    Jimp.read(`${folder.location}\\${fileContainer.image.imageName}`)
                    .then((file) => {
                        // ImageOptimizer.progressBar.setText('Reading files..');
                        const imageSize = scale ? 
                            file.bitmap.width > file.bitmap.height ? 
                                {w: size, h: Jimp.AUTO} 
                                : {w: Jimp.AUTO, h: size}
                            : {w: file.bitmap.width, h: Jimp.AUTO};
                        fileIndex++;
                        let defaultType = '';
                        if(changeFileType) {
                            if(file.getMIME() == 'image/jpeg') defaultType = '.jpg';
                            else defaultType = '.png';
                        } else defaultType = '.' + fileType;
                        
                        file.resize(imageSize.w, imageSize.h)
                        .quality(quality)
                        .write(`${destination}${renameFiles ? (fileIndex + fileName + defaultType) : fileContainer.image.imageName}`, () => {
                            // ImageOptimizer.progressBar.setText('Writing files..');
                            currentFile++;
                            ImageOptimizer.progressBar.setValue((currentFile/totalFiles)*100);
                            console.log(`Writing file ${currentFile}/${totalFiles}`);
                            if(currentFile == totalFiles)
                                document.querySelector('.preview-panel').style.display = 'none';
                        });
                    }).catch(console.log);
                }
            });
        });
    }

}