module.exports = class Button {
    constructor(icon, text, classList='') {
        this.text = text;
        this.classList = classList;
        this.icon = document.createElement('i');
        this.icon.classList = 'material-icons md-48';
        this.icon.innerText = icon;
        this.html = document.createElement('button');
        this.html.classList = this.classList;
        this.html.innerHTML = `<span>${this.text}</span>`;
        this.html.appendChild(this.icon);
    }
    
    click(action) { this.html.addEventListener('click', action); }
}