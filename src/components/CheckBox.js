// module.exports = class CheckBox {
//     constructor(id, text, style='') {
//         this.text = text;
//         this.id = id;
//         this.checkBox = `<input id="${this.id}" class="checkbox" type="checkbox" /><span class="checkmark"></span>`;
//     }
//     render() {
//         const html = `<div class="label checkbox-container" style="${this.style}"><span>${this.text}</span> ${this.checkBox}</div>`
//         return html;
//     }
// }
module.exports = class CheckBox {
    constructor(checked, text, classList='') {
        this.classList = classList;

        let container = document.createElement('div');
        container.classList = 'label checkbox-container';

        const textSpan = document.createElement('span');
        textSpan.innerText = text;
        textSpan.className = 'checkbox-label';

        const checkBox = document.createElement('input');
        checkBox.setAttribute('type', 'checkbox');
        checkBox.checked = checked;
        checkBox.classList = classList + ' checkbox';

        const checkMark = document.createElement('span');
        checkMark.className = 'checkmark';

        container.appendChild(textSpan);
        container.appendChild(checkBox);
        container.appendChild(checkMark);
        this.html = container;
    }
    
    change(action) { this.html.addEventListener('change', action); }
}