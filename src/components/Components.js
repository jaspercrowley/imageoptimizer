module.exports = class Components {
    constructor(){
        this.Panel = require('../components/Panel');
        this.Button = require('../components/Button');
        this.ImagesPanel = require('../components/ImagesPanel');
        this.TabWindow = require('../components/TabWindow');
        this.CheckBox = require('../components/CheckBox');
        this.DropDown = require('../components/DropDown');
        this.Label = require('../components/Label');
        this.Range = require('../components/Range');
        this.TextBox = require('../components/TextBox');
        this.NumberBox = require('../components/NumberBox');
        this.Folder = require('../components/Folder');
        this.Picture = require('../components/Picture');
        this.ImageContainer = require('../components/ImageContainer');
        this.Progress = require('../components/Progress');
    }
}
    
        
        
    

