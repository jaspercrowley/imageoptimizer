module.exports = class DropDown {
    constructor(text, value, options, classList='') {
        const container =  document.createElement('div');
        container.className = 'dropdown-container';
        const label = document.createElement('span');
        label.className = 'label';
        label.innerText = text;
        container.appendChild(label);

        const dropDown = document.createElement('select');
        dropDown.classList = classList;
        dropDown.value = value;

        options.forEach(option => {
            const newOption = document.createElement('option');
            newOption.value = option.toLowerCase();
            newOption.innerText = option;
            dropDown.appendChild(newOption);
        });

        container.appendChild(dropDown);
        
        this.html = container;
    }
    
    change(action) { this.html.addEventListener('change', action); }
}