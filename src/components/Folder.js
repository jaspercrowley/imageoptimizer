const Components = require('../components/Components');
const { Button, Label, Panel } = new Components();

module.exports = class Folder {
    constructor(location, text, files) {
        this.location = location;
        this.text = text;
        const textSpan = document.createElement('span');
        textSpan.innerText = text;
        textSpan.className = 'folder-label';
        this.files = files;
        const folder = document.createElement('div');
        folder.className = 'folder';
        const fileContainer = document.createElement('div');
        fileContainer.className = 'file-container';
        this.files.forEach((file) => fileContainer.appendChild(file.html));
        folder.appendChild(fileContainer);
        this.html = folder;
    }

    static Folders() {
        Console.log(this);
    }

    static filterFiles(files=[], extensions=[]) {
        let filteredFiles = [];
        console.log(files);
        files.forEach(file => {
            extensions.forEach(extension => {
                if(file.toLowerCase().endsWith(extension)) filteredFiles.push(file);
            });
        });
        return filteredFiles;
    }

    static findFolder(folderName) {
        let isFolder;
        if(Folder.folders.length > 0) {
            isFolder = Folder.folders.find(folder => { return folder.location == folderName; });
        } 
        return isFolder;
    }
}