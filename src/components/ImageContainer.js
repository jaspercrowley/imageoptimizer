const Components = require('../components/Components');
const {Panel, Button} = new Components();

module.exports = class ImageContainer extends Panel{
    constructor(tab, image, classList='', components) {
        super(tab, classList, components)
        this.image = image;
    }
    click(action) {this.html.addEventListener('click', action);}
}