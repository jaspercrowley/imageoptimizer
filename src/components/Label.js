module.exports = class Label {
    constructor(type, icon, text, classList='') {
        this.classList = classList;
        this.icon = document.createElement('i');
        this.icon.classList = 'material-icons md-48';
        this.icon.innerText = icon + ' ';
        this.text = document.createElement(type);
        this.text.innerText = text;
        this.html = document.createElement('div');
        this.html.classList = this.classList;
        this.html.appendChild(this.icon);
        this.html.appendChild(this.text);
    }
    
}