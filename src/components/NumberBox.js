module.exports = class NumberBox {
    constructor(text, min, max, value, classList='') {
        this.min = min;
        this.max = max;
        this.value = value;
        const container =  document.createElement('div');
        container.className = 'numberbox-container';
        const label = document.createElement('span');
        label.innerText = text;
        container.appendChild(label);

        const textBox = document.createElement('input');
        textBox.setAttribute('type', 'number');
        textBox.classList = classList;
        textBox.value = value;
        textBox.max = max;
        textBox.min = min;
        textBox.maxLength = 4;
        container.appendChild(textBox);
        
        this.html = container;
    }
    
    input(action) { this.html.addEventListener('input', action); }
    click(action) { this.html.addEventListener('click', action); }

}