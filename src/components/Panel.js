module.exports = class Panel {
    constructor(tab, classList='', components) {
        this.classList = classList;
        this.tab = tab;
        this.html = document.createElement('div');
        this.html.classList = this.classList;
        components.forEach(component => this.html.appendChild(component));
    }

    animate(show) {
        show
        ? this.html.style.animation = 'panelGrow 0.3s ease forwards'
        : this.html.style.animation = 'panelShrink 0.1s ease forwards';
    }

}