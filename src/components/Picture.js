module.exports = class Picture extends Image {
    constructor(folder, imageName) {
        super();
        this.folder = folder;
        this.imageName = imageName;
        this.quality = 75;
        this.setAttribute('src', 'images/loading.gif');
        this.classList = 'animated zoomIn';
        const randomValue = Math.random();
        this.onload = () => { this.src = this.getAttribute('data-src'); }
        this.style = `animation-duration: ${randomValue}s; animation-delay: 0.3s;`;
    }
}