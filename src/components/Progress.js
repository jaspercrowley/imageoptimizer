module.exports = class Progress {
    constructor(value, text) {
        const container = document.createElement('div');
        container.className = 'progress-container';
        const progressText = document.createElement('span');
        progressText.className = 'progress-text';
        progressText.innerText = text;
        const barBack = document.createElement('div');
        barBack.className = 'progress-bar-back';
        this.bar = document.createElement('div');
        this.bar.className = 'progress-bar';
        this.bar.style.width = `${value}%`;
        barBack.appendChild(this.bar);
        container.appendChild(progressText);
        container.appendChild(barBack);
        this.html = container;
    }

    setValue(value) {
        value != 100 ? this.bar.style.width = `${value}%` : this.bar.style.width = '0%';
    }

    setText(text) { this.progressText.innerText = text; }
}