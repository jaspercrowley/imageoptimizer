module.exports = class Range {
    constructor(value, max, min, classList='') {
        this.classList = classList;
        this.html = document.createElement('input');
        this.html.setAttribute('type', 'range');
        this.html.value = value;
        this.html.max = max;
        this.html.min = min;
        this.html.classList = this.classList;
    }
    
    input(action) { this.html.addEventListener('input', action); }
}