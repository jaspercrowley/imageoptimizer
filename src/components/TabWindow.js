module.exports = class TabWindow {
    constructor(classList='') {
        this.classList = classList;
        this.components = [];
        this.events = [];
        this.settings = {};
        this.html = document.createElement('div');
        this.html.classList = this.classList;
    }

    loadComponents() { this.components.forEach(component => this.html.appendChild(component)); }

}