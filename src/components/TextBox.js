module.exports = class TextBox {
    constructor(placeholder, text, classList='') {
        const textBox = document.createElement('input');
        textBox.setAttribute('type', 'text');
        textBox.placeholder = placeholder;
        textBox.classList = classList;
        textBox.value = text;
        this.html = textBox;
    }
    
    input(action) { this.html.addEventListener('input', action); }
    click(action) { this.html.addEventListener('click', action); }
}